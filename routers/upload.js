const express = require("express");
const router = express.Router();
const multer = require("multer");

var storage = multer.diskStorage({
  destination: "uploads",
  filename: function (req, file, cb) {
    console.log("file", file);
    cb(null, `${Date.now()}_${file.originalname}`);
  },
});
const upload = multer({ storage });

router.post("/img", upload.single("file"), function (req, res, next) {
  // req.file is the `avatar` file
  // req.body will hold the text fields, if there were any

  res.json({
    originalname: req.file.originalname,
    path: req.file.path,
    size: req.file.size,
  });
});

module.exports = router;
