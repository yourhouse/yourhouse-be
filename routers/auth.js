const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config");
const { auth } = require("../helpers/auth");
const jwtSignature = config.get("jwtSignature");

const User = require("../models/user");

router.post("/signup", async (req, res) => {
  const { username, password, email, phone, address, IDcard } = req.body;
  try {
    const foundUser = await User.findOne().or([
      { username },
      { email },
      { phone },
      { IDcard },
    ]);
    if (foundUser)
      res.status(401).send({ message: "This user already existed!" });

    const newUser = new User({
      username,
      password,
      email,
      phone,
      address,
      IDcard,
      role: "user",
    });

    let result = await newUser.save();
    result = result.toObject();
    delete result.password;
    res.status(200).send({ result });
  } catch (err) {
    console.log(err);
    res.status(401).send({ message: "Error System" });
  }
});

router.post("/signin", async (req, res) => {
  const { username, password } = req.body;
  const foundedUser = await User.findOne({ username });
  console.log("username:", username);
  if (!foundedUser) {
    return res.status(401).send({ message: "Wrong username or password 1" });
  }
  console.log("password1:", password);
  const isMatch = await bcrypt.compare(password, foundedUser.password);
  console.log("password2:", password);
  console.log("password3:", foundedUser.password);
  console.log("isMatch:", isMatch);
  if (!isMatch)
    return res.status(401).send({ message: "Wrong username or password 2" });

  console.log("jwtSignature:", jwtSignature);
  console.log("foundedUser._id:", foundedUser._id);
  const token = await jwt.sign(
    {
      _id: foundedUser._id,
    },
    jwtSignature
  );

  console.log("token:", token);
  foundedUser.tokens.push(token);
  await foundedUser.save();
  res.send(token);
});

module.exports = router;
