//library module
const express = require("express");
const router = express.Router();

//models module
const BDS = require("../models/BDS");

//controllers module
const ctrBDS = require("../controllers/BDS");

//routers
router.get("", ctrBDS.getBDS);
router.get("/getTopView", ctrBDS.getTopViewBDS);
router.get("/getDefaultHouseSale", ctrBDS.getDefaultHouseSale);
router.get("/getDefaultHouseRent", ctrBDS.getDefaultHouseRent);
router.get("/getDefaultDepartmentSale", ctrBDS.getDefaultDepartmentSale);
router.get("/getDefaultDepartmentRent", ctrBDS.getDefaultDepartmentRent);
router.get("/getDefaultLandSale", ctrBDS.getDefaultLandSale);

router.post("", ctrBDS.postBDS);
router.patch("", ctrBDS.patchBDS);
router.delete("", ctrBDS.deleteBDS);

module.exports = router;
