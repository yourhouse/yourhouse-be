const express = require("express");
const router = express.Router();

const PlaceForRent = require("../models/placeforrent");

router.post("/placeforrent", async (req, res) => {
  try {
    const {
      images,
      title,
      describe,
      furniture,
      price,
      status,
      bdsid,
      userid,
    } = req.body;
    //check existed item
    const foundPlaceForRent = await PlaceForRent.findOne({ bdsid });
    if (foundPlaceForRent)
      return res.status(401).send({ message: "BDS already existed" });

    const newPlaceForRent = new PlaceForRent({
      images,
      title,
      describe,
      furniture,
      price,
      status,
      bdsid,
      userid,
    });

    const result = await newPlaceForRent.save();
    res.send(result);
  } catch (err) {
    console.log(err);
    res.status(401).send({ message: "Error System" });
  }
});

router.get("/placeforrent", async(req,res)=>{
  const {}
});

module.exports = router;
