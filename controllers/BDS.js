const { numberParser } = require("config/parser");
const res = require("express/lib/response");
const BDS = require("../models/BDS");

const postBDS = async (req, res) => {
  try {
    const {
      //images
      address,
      location,
      ultilities,
      typetrading,
      typerealestate,
      price,
      nameowner,
      nameagency,
      emailowner,
      emailagency,
      phoneowner,
      phoneagency,
      termoptions,
      detailhost,
      view,
    } = req.body;
    //check existed
    // const foundBDS = await BDS.findOne({ code });
    // if (foundBDS)
    //   return res.status(400).send({ message: "BDS already exsited" });

    const newBDS = new BDS({
      //images
      address,
      location,
      ultilities,
      typetrading,
      typerealestate,
      price,
      nameowner,
      nameagency,
      emailowner,
      emailagency,
      phoneowner,
      phoneagency,
      termoptions,
      detailhost,
      view,
    });
    const result = await newBDS.save();
    res.status(200).send(result);
  } catch (err) {
    console.log(err);
    res.status(500).send({ message: "Error System" });
  }
};
// find All BDS
const getBDS = async (req, res) => {
  try {
    const foundBDS = await BDS.find();
    res.send(foundBDS);
  } catch (err) {
    console.log(err);
    res.status(401).send({ message: "Error System" });
  }
};
// find Top BDS
const getTopViewBDS = async (req, res) => {
  try {
    const limit = req.query.limit;
    Number(limit);
    console.log(Number(limit));
    const foundBDS = await BDS.find().sort({ view: -1 }).limit(Number(limit));
    res.send(foundBDS);
  } catch (err) {
    console.log(err);
    res.status(400).send({ message: "Bad Request" });
  }
};
//find HomePageHouseBDS
const getDefaultHouseSale = async (req, res) => {
  try {
    const limit = req.query.limit;
    Number(limit);
    const foundBDS = await BDS.find({
      $and: [{ typerealestate: "House" }, { typetrading: "Sell" }],
    })
      .limit(Number(limit))
      .sort({ view: -1 });
    res.send(foundBDS).status(200);
  } catch (err) {
    console.log(err);
    res.send({ message: "Bad Request" }).status(400);
  }
};
const getDefaultHouseRent = async (req, res) => {
  try {
    const limit = req.query.limit;
    Number(limit);
    const foundBDS = await BDS.find({
      $and: [{ typerealestate: "House" }, { typetrading: "Rent" }],
    })
      .limit(Number(limit))
      .sort({ view: -1 });
    res.send(foundBDS).status(200);
  } catch (err) {
    console.log(err);
    res.send({ message: "Bad Request" }).status(400);
  }
};
//find HomePageDepartmentBDS

const getDefaultDepartmentSale = async (req, res) => {
  try {
    const limit = req.query.limit;
    Number(limit);
    const foundBDS = await BDS.find({
      $and: [{ typerealestate: "Department" }, { typetrading: "Sell" }],
    })
      .limit(Number(limit))
      .sort({ view: -1 });
    res.send(foundBDS).status(200);
  } catch (err) {
    console.log(err);
    res.send({ message: "Bad Request" }).status(400);
  }
};
const getDefaultDepartmentRent = async (req, res) => {
  try {
    const limit = req.query.limit;
    Number(limit);
    const foundBDS = await BDS.find({
      $and: [{ typerealestate: "Department" }, { typetrading: "Rent" }],
    })
      .limit(Number(limit))
      .sort({ view: -1 });
    res.send(foundBDS).status(200);
  } catch (err) {
    console.log(err);
    res.send({ message: "Bad Request" }).status(400);
  }
};

//find HomePageLandBDS

const getDefaultLandSale = async (req, res) => {
  try {
    const limit = req.body.limit;
    Number(limit);
    const foundBDS = await BDS.find({
      $and: [{ typerealestate: "Land" }, { typetrading: "Sell" }],
    })
      .sort({ view: -1 })
      .limit(Number(limit));
    res.send(foundBDS).status(200);
  } catch (e) {
    res.send({ message: "Bad Request", e }).status(400);
  }
};

const patchBDS = async (req, res) => {
  try {
    const {
      address,
      location,
      acreage,
      bathroom,
      bedroom,
      ultilities,
      type,
      code,
      owner,
    } = req.body;
    const foundBDS = await BDS.findOne({ code });
    if (!foundBDS)
      return res.status(401).send({ message: "Can not find this" });
    foundBDS.address = address;
    foundBDS.location = location;
    foundBDS.acreage = acreage;
    foundBDS.bathroom = bathroom;
    foundBDS.bedroom = bedroom;
    foundBDS.ultilities = ultilities;
    foundBDS.type = type;
    foundBDS.owner = owner;
    const result = await foundBDS.save();
    res.status(401).send(result);
  } catch (err) {
    console.log(err);
    res.status(401).send({ message: "Error System" });
  }
};

const deleteBDS = async (req, res) => {
  try {
    const { code } = req.query;
    await BDS.findOneAndDelete({ code });
    res.status(200).send({ message: "Success" });
  } catch (err) {
    console.log(err);
    res.status(401).send({ message: "Error System" });
  }
};

module.exports = {
  postBDS,
  getBDS,
  patchBDS,
  deleteBDS,
  getTopViewBDS,
  getDefaultHouseSale,
  getDefaultHouseRent,
  getDefaultDepartmentSale,
  getDefaultDepartmentRent,
  getDefaultLandSale,
};
