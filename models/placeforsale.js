const mongoose = require("mongoose");

const placeForSaleSchema = new mongoose.Schema({
  title: String,
  describe: String,
  furniture: String,
  price: Number,
  status: String,
  dateupload: new Date(),
  bdsid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "BDS",
  },
  userid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const PlaceForSale = mongoose.model("PlaceForSale", placeForSaleSchema);
module.exports = PlaceForSale;
