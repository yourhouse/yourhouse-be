const mongoose = require("mongoose");

const plantForSaleSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: String,
  furniture: String,
  price: Number,
  status: String,
  dateupload: new Date(),
  bdsid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "BDS",
  },
  userid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const plantForSale = mongoose.model(
  "plantForSale",
  plantForSaleSchema,
  "plantForSales"
);
module.exports = plantForSale;
