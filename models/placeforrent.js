const mongoose = require("mongoose");

const placeForRentSchema = new mongoose.Schema({
  title: String,
  describe: String,
  furniture: String,
  price: Number,
  status: String,
  dateupload: new Date(),
  bdsid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "BDS",
  },
  userid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const PlaceForRent = mongoose.model("PlaceForRent", placeForRentSchema);
module.exports = PlaceForRent;
