const mongoose = require("mongoose");

const departmentForRentSchema = mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: String,
  furniture: String,
  price: Number,
  status: String,
  dateupload: new Date(),
  bdsid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "BDS",
  },
  userid: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const DepartmentForRent = mongoose.model(
  "DepartmentForRent",
  departmentForRentSchema,
  "DepartmentForRents"
);
module.exports = DepartmentForRent;
