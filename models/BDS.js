const mongoose = require("mongoose");
const config = require("config");

const publicFile = config.get("publicFile");

const BDSSchema = new mongoose.Schema(
  {
    address: {
      type: String,
      required: true,
    },
    // images: {
    //   type: [
    //     {
    //       path: String,
    //       originalname: String,
    //       size: Number,
    //     },
    //   ],
    //   get: (imgs) => {
    //     return imgs.map((item) => {
    //       return {
    //         path: item.path,
    //         originalname: item.originalname,
    //         size: item.size,
    //         link: `${publicFile}/${item.path}`,
    //       };
    //     });
    //   },
    //   default: [],
    //   required: true,
    // },
    location: {
      type: String,
      required: true,
    },
    acreage: {
      type: Number,
      require: true,
    },
    bathroom: Number,
    bedroom: Number,
    ultilities: [String],
    typetrading: String,
    typerealestate: String,
    price: Number,
    view: {
      type: Number,
      default: 0,
    },
    nameowner: String,
    nameagency: String,
    emailowner: String,
    emailagency: String,
    phoneowner: String,
    phoneagency: String,
    termoptions: [String],
    detailhost: String,
  },
  {
    timestamps: true,
    toJSON: {
      getters: true,
    },
  }
);

// const root = "https://localhost:3000/";
// BDSSchema.path("images.0.path").get((v) => `${root}${v}`);

const BDS = mongoose.model("BDS", BDSSchema);

module.exports = BDS;
