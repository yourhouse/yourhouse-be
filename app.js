const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const config = require("config");
// const jwt = require("jsonwebtoken");
require("./db/connect");
const authRouter = require("./routers/auth");
const BDSRouter = require("./routers/BDS");
const uploadRouter = require("./routers/upload");

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(authRouter);
app.use("/BDS", BDSRouter);
app.use("/upload", uploadRouter);

app.use("/uploads", express.static("uploads"));
// app.use(express.static("uploads"));

const PORT = process.env.PORT || config.get("port");
app.listen(PORT, () => {
  console.log(`listening..... on port: ${PORT}`);
});
